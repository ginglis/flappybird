// FlappyBird.cpp
// implementation of the FlappyBird class for final project
// Gavin Inglis
// 12/4/18

#include<fstream>
#include<cmath>
#include<cstdlib>
#include<unistd.h>
#include<string>
using namespace std;
#include "gfx2.h"
#include "flappyBird.h"


FlappyBird::FlappyBird(){
  bird.y=300; // initialize bird y
  bird.change=1.0; // initialize gravity
}

FlappyBird::~FlappyBird(){}

void FlappyBird::setScore(int s){
  score = s; // method used when player wants to play again to set score to 0
}

int FlappyBird::getScore(){
  return score;
}

void FlappyBird::setBirdY(int y){
  bird.y=y;
}

void FlappyBird::setBirdChange(float c){
  bird.change=c;
}


void FlappyBird::initPipes(){
  srand(time(NULL)); // initalize seed for random num generation
  // random nums used to generate height of each bottom pipe
  // the height of the top pipes is calculated based off of this
  // random height, all pairs of pipes have the same distance between them

  // first set of pipes begin at width of screen
  bp1.x=SCREEN_WIDTH;
  bp1.y=rand()%550+SPACE;
  bp1.height=SCREEN_HEIGHT-bp1.y;
  tp1.x=SCREEN_WIDTH;
  tp1.y=TOP_Y;
  tp1.height=SCREEN_HEIGHT-bp1.height-SPACE;

  // second set of pipes spaced to the right of first set
  bp2.x=bp1.x+SCREEN_WIDTH/3+27; // add 27 so the x pos is divisible by 5. This comes into play when checking for score
  bp2.y=rand()%550+SPACE;
  bp2.height=SCREEN_HEIGHT-bp2.y;
  tp2.x=tp1.x+SCREEN_WIDTH/3+27;
  tp2.y=TOP_Y;
  tp2.height=SCREEN_HEIGHT-bp2.height-SPACE;

  // third set of pipes spaced to right of second set
  bp3.x=bp2.x+SCREEN_WIDTH/3+27;
  bp3.y=rand()%550+SPACE;
  bp3.height=SCREEN_HEIGHT-bp3.y;
  tp3.x=tp2.x+SCREEN_WIDTH/3+27;
  tp3.y=TOP_Y;
  tp3.height=SCREEN_HEIGHT-bp3.height-SPACE;

}

void FlappyBird::drawPipes(){
  gfx_color(66,255,116);

  gfx_fill_rectangle(bp1.x,bp1.y,bp1.width,bp1.height);
  gfx_fill_rectangle(tp1.x,tp1.y,tp1.width,tp1.height);

  gfx_fill_rectangle(bp2.x,bp2.y,bp2.width,bp2.height);
  gfx_fill_rectangle(tp2.x,tp2.y,tp2.width,tp2.height);

  gfx_fill_rectangle(bp3.x,bp3.y,bp3.width,bp3.height);
  gfx_fill_rectangle(tp3.x,tp3.y,tp3.width,tp3.height);

}

void FlappyBird::scrollPipes(){
  srand(time(NULL)); // seed again to
  // move x pos to the left to scroll pipes
  tp1.x-=5;
  bp1.x-=5;

  tp2.x-=5;
  bp2.x-=5;

  tp3.x-=5;
  bp3.x-=5;

  // conditionals to check if the pipe is off screen
  // if pipe is off screen, reset it to all the way to the right of the screen
  if(tp1.x+tp1.width<=0||bp1.x+bp1.width<=0){
    bp1.x=SCREEN_WIDTH;
    bp1.y=rand()%550+SPACE;
    bp1.height=SCREEN_HEIGHT-bp1.y;
    tp1.x=SCREEN_WIDTH;
    tp1.y=TOP_Y;
    tp1.height=SCREEN_HEIGHT-bp1.height-SPACE;
  }

  if(tp2.x+tp2.width<=0||bp2.x+bp2.width<=0){
    bp2.x=bp1.x+SCREEN_WIDTH/3+27;
    bp2.y=rand()%550+SPACE;
    bp2.height=SCREEN_HEIGHT-bp2.y;
    tp2.x=tp1.x+SCREEN_WIDTH/3+27;
    tp2.y=TOP_Y;
    tp2.height=SCREEN_HEIGHT-bp2.height-SPACE;
  }

  if(tp3.x+tp3.width<=0||bp3.x+bp3.width<=0){
    bp3.x=bp2.x+SCREEN_WIDTH/3+27;
    bp3.y=rand()%550+SPACE;
    bp3.height=SCREEN_HEIGHT-bp3.y;
    tp3.x=tp2.x+SCREEN_WIDTH/3+27;
    tp3.y=TOP_Y;
    tp3.height=SCREEN_HEIGHT-bp3.height-SPACE;
  }
}

void FlappyBird::drawBird(){
  gfx_color(244,232,66);
  gfx_fill_circle(bird.x,bird.y,bird.radius);

  // bird 'eye'
  gfx_color(0,0,0);
  gfx_point(bird.x+3,bird.y-3);
  gfx_point(bird.x+4,bird.y-3);
  gfx_point(bird.x+3,bird.y-4);
  gfx_point(bird.x+4,bird.y-4);
}

void FlappyBird::jump(){
  bird.y-=35; // the jump
  bird.change=1.0; // reset gravity
}

void FlappyBird::gravity(){
  bird.change+=3;
  bird.y+=bird.change;
}

void FlappyBird::incScore(){
  // works b/c at some point in time, bird.x will equal all of these position (if it passes through)
  if(bird.x==bp1.x+bp1.width||bird.x==bp2.x+bp2.width||bird.x==bp3.x+tp3.width){
    score+=1;
  }
}

bool FlappyBird::gameOver(){
  // check if bird falls to ground
  if(bird.y+bird.radius>=SCREEN_HEIGHT) return true;

  // check collision along side of pipes
  if(bird.x+bird.radius==tp1.x){
    for(int i=0;i<tp1.height;i++){
      if(bird.y-bird.radius==tp1.y+i){
        return true;
      }
    }
  }

  if(bird.x+bird.radius==bp1.x){
    for(int i=0;i<bp1.height;i++){
      if(bird.y+bird.radius==bp1.y+i){
        return true;
      }
    }
  }

  if(bird.x+bird.radius==tp2.x){
    for(int i=0;i<tp2.height;i++){
      if(bird.y-bird.radius==tp2.y+i){
        return true;
      }
    }
  }

  if(bird.x+bird.radius==bp2.x){
    for(int i=0;i<bp2.height;i++){
      if(bird.y+bird.radius==bp2.y+i){
        return true;
      }
    }
  }

  if(bird.x+bird.radius==tp3.x){
    for(int i=0;i<tp3.height;i++){
      if(bird.y-bird.radius==tp3.y+i){
        return true;
      }
    }
  }

  if(bird.x+bird.radius==bp3.x){
    for(int i=0;i<bp3.height;i++){
      if(bird.y+bird.radius==bp3.y+i){
        return true;
      }
    }
  }

  // check along width of pipes
  if(bird.y-bird.radius<=tp1.y+tp1.height){
    for(int i=0;i<tp1.width;i++){
      if(bird.x==tp1.x+i){
        return true;
      }
    }
  }

  if(bird.y+bird.radius>=bp1.y){
    for(int i=0;i<bp1.width;i++){
      if(bird.x==bp1.x+i){
        return true;
      }
    }
  }

  if(bird.y-bird.radius<=tp2.y+tp2.height){
    for(int i=0;i<tp2.width;i++){
      if(bird.x==tp2.x+i){
        return true;
      }
    }
  }

  if(bird.y+bird.radius>=bp2.y){
    for(int i=0;i<bp2.width;i++){
      if(bird.x==bp2.x+i){
        return true;
      }
    }
  }

  if(bird.y-bird.radius<=tp3.y+tp3.height){
    for(int i=0;i<tp3.width;i++){
      if(bird.x==tp3.x+i){
        return true;
      }
    }
  }

  if(bird.y+bird.radius>=bp3.y){
    for(int i=0;i<bp3.width;i++){
      if(bird.x==bp3.x+i){
        return true;
      }
    }
  }

  return false;
}


void FlappyBird::gameOverDisp(){
  gfx_clear();
  gfx_color(245,245,220); //beige
  int rectX=SCREEN_WIDTH/2-SPACE;
  int rectY=SCREEN_HEIGHT/2-SPACE/2;
  gfx_fill_rectangle(rectX,rectY,SPACE*2,SPACE);
  gfx_color(0,0,0);
  gfx_text(rectX+125,rectY+25,"GAME OVER.");

  string s = to_string(score);
  char const *cs =s.c_str();
  gfx_text(rectX+125,rectY+50,"SCORE: ");
  gfx_text(rectX+200,rectY+50,cs);


  gfx_text(rectX+75,rectY+125,"P: PLAY AGAIN");
  gfx_text(rectX+175,rectY+125,"Q: QUIT");

  // read in high score to display
  ifstream ifs;
  ifs.open("highscore.txt");
  string hs;
  getline(ifs,hs);
  ifs.close();
  char const *chs = hs.c_str();


  int highScore=stoi(hs);

  if(score>=highScore){ // this is >= so that a new high score will display as both the player's score and the high score
    gfx_text(rectX+115,rectY+100,"NEW HIGH SCORE!");
    chs=cs;
  }

  if(score>highScore){ // so that it only writes to file once upon a new high score
    ofstream ofs;
    ofs.open("highscore.txt");
    ofs << to_string(score);
    ofs.close();
  }

  gfx_text(rectX+115,rectY+75,"HIGH SCORE: ");
  gfx_text(rectX+200,rectY+75,chs);

  gfx_flush();
}

void FlappyBird::startMenu(){
  // one of the last methods i added
  // start menu to select playing or quitting
  gfx_clear_color(66,226,244);
  gfx_color(245,245,220);
  int rectX=SCREEN_WIDTH/2-SPACE;
  int rectY=SCREEN_HEIGHT/2-SPACE/2;
  gfx_fill_rectangle(rectX,rectY,SPACE*2,SPACE);
  gfx_color(0,0,0);
  gfx_text(rectX+85,rectY+25,"WELCOME TO FLAPPY BIRD!");
  gfx_text(rectX+100,rectY+50,"PRESS SPACE TO JUMP");
  gfx_text(rectX+100,rectY+100,"P: PLAY");
  gfx_text(rectX+175,rectY+100,"Q: QUIT");
  // displays the most recent high score to the player
  ifstream ifs;
  ifs.open("highscore.txt");
  string hs;
  getline(ifs,hs);
  ifs.close();
  char const *chs = hs.c_str();

  gfx_text(rectX+115,rectY+75,"HIGH SCORE: ");
  gfx_text(rectX+190,rectY+75,chs);
}
