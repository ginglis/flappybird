# C++ Flappy Bird Clone 



For this project, I created my a class for the objects of the game and used a provided graphics library. Within my class are two
structs: one for a pipe and one for the bird. A difficult part of this project was generating
pipes of random height and generating these pipes endlessly. To create a pair of pipes, I first generate a random height for the bottom pipe.
The class contains a constant integer for the distance between pipes, so I just do a bit of math based on the screen size to calculate the height
of the top pipe. There are a maximum of three pairs of pipes (6 pipe objects) on screen at a time. To endlessly loop the pipes, I check if a pipe
has reached the leftmost part of the screen. If it has, I simply change the x-position of the pair of pipes to be a certain distance behind
the current rightmost pair of pipes and generate a new, random height for the pipes. This way, the pipes endlessly cycle to the "back of the line". 
The graphics are being run using [XQuartz](https://www.xquartz.org/), which more or less mimics Apples old X11 features using an X server.
    
    
If you wish to use this project, go ahead. The Makefile is provided. All you need to do is have XQuartz installed on your machine.
If the path to your X11 library is different than mine, you may need to change the Makefile. However, I believe mine was linked to
the default download location.

I hope to port this clone to my personal site as an embedded game. I plan on doing this by either
rewriting some of the code in JS or using [Ultralight](https://ultralig.ht/).


![](flappyBird.gif)