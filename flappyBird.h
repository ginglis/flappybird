// flappyBird.h
// class for the flappy bird game, final project
// Gavin Inglis
// 12/4/18

const int TOP_Y=0;
const int SCREEN_WIDTH=700;
const int SCREEN_HEIGHT=700;
const int SPACE = 150;

struct Bird{
  const int x=350;
  int y;
  const int radius=10;
  float change; // used for acceleration/gravity
};

struct Pipe{
  int x;
  int y;
  const int width=50;
  int height;
};

class FlappyBird{
  public:
    FlappyBird();
    ~FlappyBird();

    void startMenu();

    void initPipes();
    void drawPipes();
    void scrollPipes(); // contains check for wrapping pipe around

    void drawBird();
    void jump();
    void gravity();

    void incScore();
    int getScore();
    void getHighScore();

    bool gameOver();
    void gameOverDisp();

   void setBirdY(int);
   void setScore(int);
   void setBirdChange(float);

  private:
    Bird bird;
    Pipe tp1;
    Pipe bp1;
    Pipe tp2;
    Pipe bp2;
    Pipe tp3;
    Pipe bp3;
    int score=0;
};
