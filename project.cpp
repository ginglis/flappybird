// project.cpp
// main driver for final project
// Gavin Inglis
// 12/6/18

#include<string>
#include<cmath>
#include<cstdlib>
#include<unistd.h>
using namespace std;
#include "gfx2.h"
#include "flappyBird.h"

int main(){
  gfx_open(SCREEN_WIDTH, SCREEN_HEIGHT, "Flappy Bird");
  bool loop=true;
  FlappyBird f;
  f.startMenu(); // display start menu
  bool again; // used for checking if the player wants to play
      char c=gfx_wait();
      switch(c){
        case 'p':
          again=true;
          break;
        case 'q':
          loop=false;
          break;
        default:
          break;
      }
  f.initPipes(); // set inital pos of the pipes

  while(loop) {
    if(!f.gameOver()&&again){
      if(gfx_event_waiting()){
        char c=gfx_wait();
        gfx_clear();
        switch(c){
          case ' ':
            f.jump(); // only input while playing is to jump
            break;
          default:
            break;
        }
      }
      else{
        gfx_clear();
        gfx_flush();
        f.drawBird();
        f.drawPipes();
        int sc = f.getScore();
        string s= to_string(sc);
        char const *scoreDisp = s.c_str();
        gfx_color(0,0,0);
        gfx_text(SCREEN_WIDTH/2,100,scoreDisp); // display current score to player
        gfx_flush();
        usleep(60000); // sleep before updating everying, i.e. score, gravity, scroll pipes
        f.incScore();
        f.gravity();
        f.scrollPipes();
        gfx_flush();
      }
   }
   else if (f.gameOver()){ // if game is over
     again=false; // playing again is false
     if(gfx_event_waiting()){
       char c=gfx_wait();
       gfx_clear();
       switch(c){
         case 'p':
           // reset all vars, play again is true. will go back to above while loop
           f.setBirdY(350);
           f.setScore(0);
           f.setBirdChange(1.0);
           f.initPipes();
           again=true;
           break;
         case 'q':
           loop=false;
         default:
           break;
       }
     }
     else{
       f.gameOverDisp(); // menu for player to quit or play again
       usleep(600000); // so that the display is static
     }
   }
  }
}
