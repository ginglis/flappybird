CMP = g++ -std=c++11 -lX11 -I/opt/X11/include/ -L/opt/X11/lib/
CLASS = flappyBird
MAIN = project
EXEC = project
GFX = gfx3_mac

$(EXEC): $(CLASS).o $(MAIN).o
	$(CMP) $(CLASS).o $(MAIN).o $(GFX).o -o $(EXEC)

$(CLASS).o: $(CLASS).cpp $(CLASS).h
	$(CMP) -c $(CLASS).cpp -o $(CLASS).o

$(MAIN).o: $(MAIN).cpp $(CLASS).h
	$(CMP) -c $(MAIN).cpp -o $(MAIN).o

clean:
	rm $(CLASS).o
	rm $(MAIN).o
	rm $(EXEC)

